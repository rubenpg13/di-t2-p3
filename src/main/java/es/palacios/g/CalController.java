package es.palacios.g;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CalController implements Initializable {

    @FXML
    private TextField operador1;
    @FXML
    private TextField operador2;
    @FXML
    private TextField resultado;
    @FXML
    private RadioButton suma;
    @FXML
    private RadioButton resta;
    @FXML
    private RadioButton mult;
    @FXML
    private RadioButton division;
    @FXML
    private Button calcular;

    private Stage escenario;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    private void handleCalcular() {
        int op1 = Integer.parseInt(operador1.getText());
        int op2 = Integer.parseInt(operador2.getText());
        if (suma.isSelected()) {
            resultado.setText(String.valueOf(suma(op1,op2)));
        } else if (resta.isSelected()) {
            resultado.setText(String.valueOf(resta(op1,op2)));
        } else if (mult.isSelected()) {
            resultado.setText(String.valueOf(mult(op1,op2)));
        } else if (division.isSelected()) {
            resultado.setText(String.valueOf(division(op1,op2)));
        }
    }

    void setEscenario(Stage escenario) {
        this.escenario = escenario;
    }

    private int suma(int op1, int op2) {
        return op1 + op2;
    }

    private int resta(int op1, int op2) {
        return op1 - op2;
    }

    private int mult(int op1, int op2) {
        return op1 * op2;
    }

    private int division(int op1, int op2) {
        if (op2 == 0) {
            return 0;
        }
        return op1 / op2;
    }
}
