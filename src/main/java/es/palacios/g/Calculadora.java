package es.palacios.g;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Calculadora extends Application {

    private Stage escenario;

    @Override
    public void start(Stage stage) throws Exception {
        this.escenario = stage;
        this.escenario.setTitle("Calculadora");
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Calculadora.class.getResource("calculadora.fxml"));
            Parent escena = loader.load();

            CalController calController = loader.getController();
            calController.setEscenario(escenario);

            escenario.setScene(new Scene(escena));
            escenario.show();
        } catch (IOException e) {
            System.err.println("Error principal!"+e.getMessage());
        }

    }

    public static void main(String[] args) {
        launch();
    }

}
